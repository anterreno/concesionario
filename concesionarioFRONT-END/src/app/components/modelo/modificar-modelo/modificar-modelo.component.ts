import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModeloService } from 'src/app/service/modelo.service';


@Component({
  selector: 'app-modificar-modelo',
  templateUrl: './modificar-modelo.component.html',
  styleUrls: ['./modificar-modelo.component.css']
})
export class ModificarModeloComponent implements OnInit {

  modelo: any = {};
  constructor(private _activate: ActivatedRoute, private _servicio: ModeloService, private _router: Router ) { }

  ngOnInit(): void {
    this._activate.params.subscribe(parametros => {this._servicio.getModelo(parametros['id']).subscribe(respuesta => {
      this.modelo = respuesta;
    })})
  }

  guardar(formulario){
    if(formulario.valid){
      this._servicio.guardar(this.modelo).subscribe((respuesta) => {console.log(respuesta);});
    }
    this._router.navigate(['/modelo']);

  }

}
