import { VersionService } from './../../../service/version.service';
import { Version } from './../../../models/version';
import { MarcaService } from './../../../service/marca.service';
import { Marca } from './../../../models/marca';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Modelo } from 'src/app/models/modelo';
import { ModeloService } from 'src/app/service/modelo.service';


@Component({
  selector: 'app-nuevo-modelo',
  templateUrl: './nuevo-modelo.component.html',
  styleUrls: ['./nuevo-modelo.component.css']
})
export class NuevoModeloComponent implements OnInit {
  marca = new Marca();
  modelo = new Modelo();
  marcas: Marca[] = [];
  constructor(private _servicio: ModeloService, private _servicioMarca: MarcaService, private _router: Router) { }

  ngOnInit(): void {
    this._servicioMarca.get().subscribe((respuesta) => {
      this.marcas = respuesta;
    });
  }

  guardar(formulario) {
    let modelo: any = {};
    modelo.nombre = formulario.value.nombre;
    modelo.marca = formulario.value.marca;
    if(formulario.valid){
      this._servicio.guardar(modelo).subscribe((respuesta) => {console.log(respuesta);});
    }
    this._router.navigate(['/modelo']);
  }


}
