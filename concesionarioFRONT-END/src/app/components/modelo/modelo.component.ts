import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Modelo } from 'src/app/models/modelo';
import { ModeloService } from 'src/app/service/modelo.service';


@Component({
  selector: 'app-modelo',
  templateUrl: './modelo.component.html',
  styleUrls: ['./modelo.component.css']
})
export class ModeloComponent implements OnInit {

  modelos: Modelo[];
  cantModelos: number;
  ids: number[] = [];

  constructor(private _servicio: ModeloService, private _router: Router) { }

  ngOnInit(): void {
    this._servicio.get().subscribe((respuesta) => {
      this.modelos = respuesta;  });
  }

  eliminar(idModelo: number) {
    this.cantModelos = this.modelos.length;
    for (var i = 0; i < this.cantModelos; i++) {
      const esta = this.ids.indexOf(this.modelos[i].idModelo);
      if (esta === -1) {
        this.ids.push(this.modelos[i].idModelo);
      }
    }
    var indice = this.ids.indexOf(idModelo);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar la versión?')) {
      console.log(idModelo);
      this._servicio.eliminar(idModelo).subscribe((respuesta) => {
       this.modelos.splice(indice,1);
      });
    location.reload();
    }
  }

  modificar(idModelo: number){
    this._router.navigate(['/modelo/modificar/' + idModelo]);
  }



}
