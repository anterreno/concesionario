import { Version } from './../../models/version';
import { Router } from '@angular/router';
import { VersionService } from './../../service/version.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.css'],
})
export class VersionComponent implements OnInit {
  versiones: Version[];
  cantVersiones: number;
  ids: number[] = [];

  constructor(
    private _servicioVersion: VersionService,
    private _router: Router,
    private _toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this._servicioVersion.get().subscribe((respuesta) => {
      this.versiones = respuesta;
    });
  }

  eliminar(id: number) {
    this.cantVersiones = this.versiones.length;
    for (var i = 0; i < this.cantVersiones; i++) {
      const esta = this.ids.indexOf(this.versiones[i].id);
      if (esta === -1) {
        this.ids.push(this.versiones[i].id);
      }
    }
    var indice = this.ids.indexOf(id);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar la versión?')) {
      console.log(id);
      this._servicioVersion.eliminar(id).subscribe((respuesta) => {
       this.versiones.splice(indice,1);

      });
      location.reload();
      // this._toastr.error('Registo eliminado exitosamente!');
    }
  }

  modificar(id: number) {
    this._router.navigate(['/version/modificar/' + id]);
  }
}
