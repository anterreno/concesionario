import { ModeloService } from './../../../service/modelo.service';
import { Modelo } from './../../../models/modelo';
import { Version } from './../../../models/version';
import { Component, OnInit } from '@angular/core';
import { VersionService } from 'src/app/service/version.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-nueva-version',
  templateUrl: './nueva-version.component.html',
  styleUrls: ['./nueva-version.component.css']
})
export class NuevaVersionComponent implements OnInit {
  version = new Version();
  modelos: Modelo[] = [];
  constructor(private _servicioVersion: VersionService, private _servicioModelo: ModeloService, private _router: Router, private _toastr: ToastrService) { }

  ngOnInit(): void {
    this._servicioModelo.get().subscribe((respuesta) => {
      this.modelos = respuesta;
    });
  }

  guardar(formulario) {
    if(formulario.valid){
      this._servicioVersion.guardar(this.version).subscribe();
      this._toastr.success('Registo guardado exitosamente!');
    }
    this._router.navigate(['/version']);
  }


}
