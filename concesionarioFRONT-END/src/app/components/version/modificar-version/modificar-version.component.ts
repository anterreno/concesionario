import { VersionService } from './../../../service/version.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-modificar-version',
  templateUrl: './modificar-version.component.html',
  styleUrls: ['./modificar-version.component.css']
})
export class ModificarVersionComponent implements OnInit {

  version: any = {};
  constructor(private _activate: ActivatedRoute, private _servicioVersion: VersionService, private _router: Router, private _toastr: ToastrService ) { }

  ngOnInit(): void {
    this._activate.params.subscribe(parametros => {this._servicioVersion.getVersion(parametros['id']).subscribe(respuesta => {
      this.version = respuesta;
    })})
  }

  guardar(formulario){
    if(formulario.valid){
      this._servicioVersion.guardar(this.version).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo modificado exitosamente!');
    }
    this._router.navigate(['/version']);

  }

}
