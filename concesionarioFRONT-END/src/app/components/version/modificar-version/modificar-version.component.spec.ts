import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarVersionComponent } from './modificar-version.component';

describe('ModificarVersionComponent', () => {
  let component: ModificarVersionComponent;
  let fixture: ComponentFixture<ModificarVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
