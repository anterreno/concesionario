import { TipoVehiculo } from './../../models/tipo-vehiculo';
import { Router } from '@angular/router';
import { TipoVehiculoService } from './../../service/tipo-vehiculo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-vehiculo',
  templateUrl: './tipo-vehiculo.component.html',
  styleUrls: ['./tipo-vehiculo.component.css']
})
export class TipoVehiculoComponent implements OnInit {

  tvehiculos: TipoVehiculo[];
  cantTvehiculos: number;
  ids: number[] = [];

  constructor(private _servicioTipoVehiculo: TipoVehiculoService, private _router: Router) { }

  ngOnInit(): void {
    this._servicioTipoVehiculo.get().subscribe((respuesta) => {
      this.tvehiculos = respuesta;  });
  }

  eliminar(id: number) {
    this.cantTvehiculos = this.tvehiculos.length;
    for (var i = 0; i < this.cantTvehiculos; i++) {
      const esta = this.ids.indexOf(this.tvehiculos[i].id);
      if (esta === -1) {
        this.ids.push(this.tvehiculos[i].id);
      }
    }
    var indice = this.ids.indexOf(id);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar el tipo de vehículo?')) {
      console.log(id);
      this._servicioTipoVehiculo.eliminar(id).subscribe((respuesta) => {
       this.tvehiculos.splice(indice,1);
      });
      location.reload();
    }
  }

  modificar(id: number){
    this._router.navigate(['/tipovehiculo/modificar/' + id]);
  }

}
