import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoTipoVehiculoComponent } from './nuevo-tipo-vehiculo.component';

describe('NuevoTipoVehiculoComponent', () => {
  let component: NuevoTipoVehiculoComponent;
  let fixture: ComponentFixture<NuevoTipoVehiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoTipoVehiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoTipoVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
