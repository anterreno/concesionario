import { Router } from '@angular/router';
import { TipoVehiculoService } from './../../../service/tipo-vehiculo.service';
import { TipoVehiculo } from './../../../models/tipo-vehiculo';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-nuevo-tipo-vehiculo',
  templateUrl: './nuevo-tipo-vehiculo.component.html',
  styleUrls: ['./nuevo-tipo-vehiculo.component.css']
})
export class NuevoTipoVehiculoComponent implements OnInit {

  vehiculo = new TipoVehiculo();
  constructor(private _servicioTipoVehiculo: TipoVehiculoService, private _router: Router,  private _toastr: ToastrService) { }

  ngOnInit(): void {
  }

  guardar(formulario) {
    if(formulario.valid){
      this._servicioTipoVehiculo.guardar(this.vehiculo).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo guardado exitosamente!');
    }
    this._router.navigate(['/tipovehiculo']);
  }

}
