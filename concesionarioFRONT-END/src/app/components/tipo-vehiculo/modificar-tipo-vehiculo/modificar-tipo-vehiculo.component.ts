import { TipoVehiculoService } from './../../../service/tipo-vehiculo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modificar-tipo-vehiculo',
  templateUrl: './modificar-tipo-vehiculo.component.html',
  styleUrls: ['./modificar-tipo-vehiculo.component.css']
})
export class ModificarTipoVehiculoComponent implements OnInit {

  vehiculo: any = {};
  constructor(private _activate: ActivatedRoute, private _servicioTipoVehiculo: TipoVehiculoService, private _router: Router, private _toastr: ToastrService ) { }

  ngOnInit(): void {
    this._activate.params.subscribe(parametros => {this._servicioTipoVehiculo.getVersion(parametros['id']).subscribe(respuesta => {
      this.vehiculo = respuesta;
    })})
  }

  guardar(formulario){
    if(formulario.valid){
      this._servicioTipoVehiculo.guardar(this.vehiculo).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo modificado exitosamente!');

    }
    this._router.navigate(['/tipovehiculo']);

  }

}
