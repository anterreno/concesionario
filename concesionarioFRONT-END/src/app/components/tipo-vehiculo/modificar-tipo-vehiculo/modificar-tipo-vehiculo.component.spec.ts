import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarTipoVehiculoComponent } from './modificar-tipo-vehiculo.component';

describe('ModificarTipoVehiculoComponent', () => {
  let component: ModificarTipoVehiculoComponent;
  let fixture: ComponentFixture<ModificarTipoVehiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarTipoVehiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarTipoVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
