import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarTipoCombustibleComponent } from './modificar-tipo-combustible.component';

describe('ModificarTipoCombustibleComponent', () => {
  let component: ModificarTipoCombustibleComponent;
  let fixture: ComponentFixture<ModificarTipoCombustibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarTipoCombustibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarTipoCombustibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
