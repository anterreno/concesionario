import { TipoCombustibleService } from 'src/app/service/tipo-combustible.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-modificar-tipo-combustible',
  templateUrl: './modificar-tipo-combustible.component.html',
  styleUrls: ['./modificar-tipo-combustible.component.css']
})
export class ModificarTipoCombustibleComponent implements OnInit {

  combustible: any = {};
  constructor(private _activate: ActivatedRoute, private _servicioTipoCombustible: TipoCombustibleService, private _router: Router,  private _toastr: ToastrService  ) { }

  ngOnInit(): void {
    this._activate.params.subscribe(parametros => {this._servicioTipoCombustible.getVersion(parametros['id']).subscribe(respuesta => {
      this.combustible = respuesta;
    })})
  }

  guardar(formulario){
    if(formulario.valid){
      this._servicioTipoCombustible.guardar(this.combustible).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo modificado exitosamente!');
    }
    this._router.navigate(['/tipocombustible']);

  }

}
