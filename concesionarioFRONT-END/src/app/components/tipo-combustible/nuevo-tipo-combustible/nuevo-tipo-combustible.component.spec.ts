import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoTipoCombustibleComponent } from './nuevo-tipo-combustible.component';

describe('NuevoTipoCombustibleComponent', () => {
  let component: NuevoTipoCombustibleComponent;
  let fixture: ComponentFixture<NuevoTipoCombustibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoTipoCombustibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoTipoCombustibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
