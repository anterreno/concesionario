import { Router } from '@angular/router';
import { TipoCombustibleService } from 'src/app/service/tipo-combustible.service';
import { TipoCombustible } from './../../../models/tipo-combustible';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-nuevo-tipo-combustible',
  templateUrl: './nuevo-tipo-combustible.component.html',
  styleUrls: ['./nuevo-tipo-combustible.component.css']
})
export class NuevoTipoCombustibleComponent implements OnInit {

  combustible = new TipoCombustible();
  constructor(private _servicioTipoCombustible: TipoCombustibleService, private _router: Router, private _toastr: ToastrService) { }

  ngOnInit(): void {
  }

  guardar(formulario) {
    if(formulario.valid){
      this._servicioTipoCombustible.guardar(this.combustible).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo guardado exitosamente!');
    }
    this._router.navigate(['/tipocombustible']);
  }

}
