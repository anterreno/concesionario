import { Router } from '@angular/router';
import { TipoCombustible } from './../../models/tipo-combustible';
import { Component, OnInit } from '@angular/core';
import { TipoCombustibleService } from 'src/app/service/tipo-combustible.service';

@Component({
  selector: 'app-tipo-combustible',
  templateUrl: './tipo-combustible.component.html',
  styleUrls: ['./tipo-combustible.component.css']
})
export class TipoCombustibleComponent implements OnInit {

  tcombustibles: TipoCombustible[];
  cantTcombustibles: number;
  ids: number[] = [];
  constructor(private _servicionTipoCombustible: TipoCombustibleService, private _router: Router) { }

  ngOnInit(): void {
    this._servicionTipoCombustible.get().subscribe((respuesta) => {
      this.tcombustibles = respuesta;  });
  }

  eliminar(id: number) {
    this.cantTcombustibles = this.tcombustibles.length;
    for (var i = 0; i < this.cantTcombustibles; i++) {
      const esta = this.ids.indexOf(this.tcombustibles[i].id);
      if (esta === -1) {
        this.ids.push(this.tcombustibles[i].id);
      }
    }
    var indice = this.ids.indexOf(id);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar el tipo de combustible?')) {
      console.log(id);
      this._servicionTipoCombustible.eliminar(id).subscribe((respuesta) => {
       this.tcombustibles.splice(indice,1);
      });
      location.reload();
    }
  }

  modificar(id: number){
    this._router.navigate(['/tipocombustible/modificar/' + id]);
  }

}
