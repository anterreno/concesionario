import { Router } from '@angular/router';
import { MotorService } from './../../service/motor.service';
import { Component, OnInit } from '@angular/core';
import { Motor } from 'src/app/models/motor';

@Component({
  selector: 'app-motor',
  templateUrl: './motor.component.html',
  styleUrls: ['./motor.component.css']
})
export class MotorComponent implements OnInit {

  motores: Motor[];
  cantMotores: number;
  ids: number[] = [];

  constructor(private _servicio: MotorService, private _router: Router) { }

  ngOnInit(): void {
    this._servicio.get().subscribe((respuesta) => {
      this.motores = respuesta;  });
  }

  eliminar(id: number) {
    this.cantMotores = this.motores.length;
    for (var i = 0; i < this.cantMotores; i++) {
      const esta = this.ids.indexOf(this.motores[i].id);
      if (esta === -1) {
        this.ids.push(this.motores[i].id);
      }
    }
    var indice = this.ids.indexOf(id);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar la versión?')) {
      console.log(id);
      this._servicio.eliminar(id).subscribe((respuesta) => {
       this.motores.splice(indice,1);
      });
    location.reload();
    }
  }

  modificar(id: number){
    this._router.navigate(['/motor/modificar/' + id]);
  }



}
