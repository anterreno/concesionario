import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MotorService } from 'src/app/service/motor.service';

@Component({
  selector: 'app-modificar-motor',
  templateUrl: './modificar-motor.component.html',
  styleUrls: ['./modificar-motor.component.css']
})
export class ModificarMotorComponent implements OnInit {

  motor: any = {};
  constructor(private _activate: ActivatedRoute, private _servicio: MotorService, private _router: Router ) { }

  ngOnInit(): void {
    this._activate.params.subscribe(parametros => {this._servicio.getMotor(parametros['id']).subscribe(respuesta => {
      this.motor = respuesta;
    })})
  }

  guardar(formulario){
    if(formulario.valid){
      this._servicio.guardar(this.motor).subscribe((respuesta) => {console.log(respuesta);});
    }
    this._router.navigate(['/motor']);

  }

}

