import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Motor } from 'src/app/models/motor';
import { MotorService } from 'src/app/service/motor.service';



@Component({
  selector: 'app-nuevo-motor',
  templateUrl: './nuevo-motor.component.html',
  styleUrls: ['./nuevo-motor.component.css']
})
export class NuevoMotorComponent implements OnInit {

  motor = new Motor();
  constructor(private _servicio: MotorService, private _router: Router) { }

  ngOnInit(): void {
  }

  guardar(formulario) {
    if(formulario.valid){
      this._servicio.guardar(this.motor).subscribe((respuesta) => {console.log(respuesta);});
    }
    this._router.navigate(['/motor']);
  }


}
