import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from 'src/app/service/token.service';
import {Cliente, ClienteService} from '../../service/cliente.service'

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  clientes:Cliente[]=[];
  cantClientes:number;
  ids:number[]=[];
  isLogged = false;
  constructor(
    private _servicioCliente: ClienteService,
    private _router: Router,
    private _tokenService: TokenService,
    private _activate: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this._servicioCliente.get().subscribe((respuesta) => {
      this.clientes = respuesta;
    });
    if (this._tokenService.getToken()) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
    }
  }

  verCliente(idCliente: number) {
    this._router.navigate(['/clientes', idCliente]);
  }
  eliminarCliente(idCliente: number) {
    this.cantClientes = this.clientes.length;
    for (var i = 0; i < this.cantClientes; i++) {
      const esta = this.ids.indexOf(this.clientes[i].idCliente);
      if (esta === -1) {
        this.ids.push(this.clientes[i].idCliente);
      }
    }
    var indice = this.ids.indexOf(idCliente);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar el cliente?')) {
      console.log(idCliente);
      this._servicioCliente.eliminarCliente(idCliente).subscribe((respuesta) => {
       this.clientes.splice(indice,1);

      });
      location.reload();
      // this._toastr.error('Registo eliminado exitosamente!');
    }
    
  }
  modificarCliente(idCliente: number) {
    this._router.navigate(['/clientes/modificar/' + idCliente]);
  }
  recargar() {
    window.location.reload();
  }
}


