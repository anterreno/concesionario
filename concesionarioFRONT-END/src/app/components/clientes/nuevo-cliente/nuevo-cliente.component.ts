import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Localidad } from 'src/app/models/localidad';
import { Cliente, ClienteService } from 'src/app/service/cliente.service';
import { TokenService } from 'src/app/service/token.service';
import { LocalidadService } from '../../../service/localidad.service';

@Component({
  selector: 'app-nuevo-cliente',
  templateUrl: './nuevo-cliente.component.html',
  styleUrls: ['./nuevo-cliente.component.css']
})
export class NuevoClienteComponent implements OnInit {
  localidades: Localidad[] = [];
  clientes:Cliente[]=[];
  constructor(
    private _router: Router,
    private _servicioCliente: ClienteService,
    private _tokenService: TokenService,
    private _toastr: ToastrService,
    private _servicioLocalidad: LocalidadService
  ) { }

  ngOnInit(): void {
    this._servicioCliente.get().subscribe((respuesta) => {
      this.clientes = respuesta;
    });
    this._servicioLocalidad.get().subscribe((respuesta) => {
      this.localidades = respuesta;
    });
  }

  guardar(miFormulario) {
    let cliente: any = {};
    cliente.nombreCliente = miFormulario.value.nombreCliente;
    cliente.apellidoCliente = miFormulario.value.apellidoCliente;
    cliente.emailCliente = miFormulario.value.emailCliente;
    cliente.direccionCliente = miFormulario.value.direccionCliente;
    cliente.dniCliente = miFormulario.value.dniCliente;
    cliente.localidad = miFormulario.value.nombreLocalidad;
    cliente.telefonoCliente = miFormulario.value.telefonoCliente;

    if (miFormulario.valid) {
      this._servicioCliente.guardarCliente(cliente).subscribe((respuesta) => {
        console.log(respuesta);
      });
      console.log(cliente);
      this._toastr.info('Datos guardados con éxito!', 'OK', {
        timeOut: 2500,
        positionClass: 'toast-top-center'
      });
      this._router.navigate(['/clientes']);
    } else {
      alert('Revise los campos');
    }
  }

}

