import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarColorComponent } from './modificar-color.component';

describe('ModificarColorComponent', () => {
  let component: ModificarColorComponent;
  let fixture: ComponentFixture<ModificarColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
