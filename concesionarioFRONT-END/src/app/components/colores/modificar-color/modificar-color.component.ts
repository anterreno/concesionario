import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ColoresService } from 'src/app/service/colores.service';

@Component({
  selector: 'app-modificar-color',
  templateUrl: './modificar-color.component.html',
  styleUrls: ['./modificar-color.component.css']
})
export class ModificarColorComponent implements OnInit {
  color: any = {};

  constructor(private activate: ActivatedRoute,
              private servicioColor: ColoresService,
              private router: Router,
              private toastr: ToastrService) {
    this.activate.params.subscribe(parametros => {this.servicioColor.getColor(parametros['idColor']).subscribe(respuesta => {
      this.color = respuesta;
    }); });
  }

  ngOnInit(): void {
  }

  modificarColor(miFormulario: NgForm) {
    let color: any = {};
    let idColor: number;
    color.nombreColor = miFormulario.value.nombreColor;
    color.codigoPintura = miFormulario.value.codigoPintura;
    idColor = this.color.idColor;
    color.idColor = this.color.idColor;
    if (miFormulario.valid) {
      this.servicioColor.modificarColor(color, idColor).subscribe(respuesta => { console.log(respuesta); });
      this.toastr.success('Modificación exitosa!', 'OK', {timeOut: 2500});
      this.router.navigate(['/colores']);
    } else {
      alert('Revise los campos');
    }
  }

}
