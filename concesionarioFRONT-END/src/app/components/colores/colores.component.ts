import { Component, OnInit } from '@angular/core';
import { TokenService } from './../../service/token.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ColoresService } from '../../service/colores.service';
import { Color } from '../../models/color';

@Component({
  selector: 'app-colores',
  templateUrl: './colores.component.html',
  styleUrls: ['./colores.component.css']
})
export class ColoresComponent implements OnInit {
  colores: Color[] = [];
  cantColores: number;
  ids: number[] = [];
  roles: string[];
  isAdmin = false;
  isLogged = false;
  isUser = false;
  constructor(
    private servicioColores: ColoresService,
    private tokenService: TokenService,
    private router: Router,
    private activate: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.activate.params.subscribe(parametros => {this.servicioColores.get().subscribe(respuesta => {
      this.colores = respuesta;
    }); });
  }

  eliminarColor(idColor: number) {
    this.cantColores = this.colores.length;
    for (var i = 0; i < this.cantColores; i++) {
      const esta = this.ids.indexOf(this.colores[i].idColor);
      if (esta === -1) {
        this.ids.push(this.colores[i].idColor);
      }
    }
    var indice = this.ids.indexOf(idColor);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar la marca?')) {
      console.log(idColor);
      this.servicioColores.eliminarColor(idColor).subscribe((respuesta) => {
       this.colores.splice(indice, 1);
      });
      location.reload();
    }
  }

  modificarColor(idColor: number) {
    this.router.navigate(['/colores/modificar-color/' + idColor]);
  }

  recargar() {
    window.location.reload();
  }


}
