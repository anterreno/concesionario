import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoColorComponent } from './nuevo-color.component';

describe('NuevoColorComponent', () => {
  let component: NuevoColorComponent;
  let fixture: ComponentFixture<NuevoColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
