import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ColoresService } from 'src/app/service/colores.service';
import { TokenService } from 'src/app/service/token.service';

@Component({
  selector: 'app-nuevo-color',
  templateUrl: './nuevo-color.component.html',
  styleUrls: ['./nuevo-color.component.css']
})
export class NuevoColorComponent implements OnInit {

  constructor( private _servicio: ColoresService,
               private _router: Router,
               private _tokenService: TokenService,
               private _toastr: ToastrService ) { }

  ngOnInit(): void {
  }

  guardarColor(miFormulario: NgForm) {
    let color: any = {};
    color.nombreColor = miFormulario.value.nombreColor;
    color.codigoPintura = miFormulario.value.codigoPintura;

    if (miFormulario.valid) {
      this._servicio.guardarColor(color).subscribe((respuesta) => {
        console.log(respuesta);
      });
      this._toastr.info('Datos guardados con éxito!', 'OK', {
        timeOut: 2500,
        positionClass: 'toast-top-center'
      });
      this._router.navigate(['/colores']);
    
    }
    
  }
}
