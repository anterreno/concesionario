import { Router } from '@angular/router';
import { CondicionService } from './../../../service/condicion.service';
import { Condicion } from './../../../models/condicion';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nueva-condicion-vehiculo',
  templateUrl: './nueva-condicion-vehiculo.component.html',
  styleUrls: ['./nueva-condicion-vehiculo.component.css']
})
export class NuevaCondicionVehiculoComponent implements OnInit {

  condicion = new Condicion();
  constructor(private _servicioCondicion: CondicionService, private _router: Router, private _toastr: ToastrService) { }

  ngOnInit(): void {
  }

  guardar(formulario) {
    if(formulario.valid){
      this._servicioCondicion.guardar(this.condicion).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo guardado exitosamente!');
    }
    this._router.navigate(['/condicion']);
  }

}
