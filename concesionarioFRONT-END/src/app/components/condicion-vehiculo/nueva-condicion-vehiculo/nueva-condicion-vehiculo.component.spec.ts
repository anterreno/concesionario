import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaCondicionVehiculoComponent } from './nueva-condicion-vehiculo.component';

describe('NuevaCondicionVehiculoComponent', () => {
  let component: NuevaCondicionVehiculoComponent;
  let fixture: ComponentFixture<NuevaCondicionVehiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaCondicionVehiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaCondicionVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
