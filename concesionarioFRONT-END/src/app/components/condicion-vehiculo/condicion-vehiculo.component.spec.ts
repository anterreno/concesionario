import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CondicionVehiculoComponent } from './condicion-vehiculo.component';

describe('CondicionVehiculoComponent', () => {
  let component: CondicionVehiculoComponent;
  let fixture: ComponentFixture<CondicionVehiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CondicionVehiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CondicionVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
