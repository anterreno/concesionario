import { CondicionService } from './../../../service/condicion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-modificar-condicion-vehiculo',
  templateUrl: './modificar-condicion-vehiculo.component.html',
  styleUrls: ['./modificar-condicion-vehiculo.component.css']
})
export class ModificarCondicionVehiculoComponent implements OnInit {

  condicion: any = {};
  constructor(private _activate: ActivatedRoute, private _servicioCondicion: CondicionService, private _router: Router, private _toastr: ToastrService ) { }

  ngOnInit(): void {
    this._activate.params.subscribe(parametros => {this._servicioCondicion.getVersion(parametros['id']).subscribe(respuesta => {
      this.condicion = respuesta;
    })})
  }

  guardar(formulario){
    if(formulario.valid){
      this._servicioCondicion.guardar(this.condicion).subscribe((respuesta) => {console.log(respuesta);});
      this._toastr.success('Registo modificado exitosamente!');

    }
    this._router.navigate(['/condicion']);

  }

}
