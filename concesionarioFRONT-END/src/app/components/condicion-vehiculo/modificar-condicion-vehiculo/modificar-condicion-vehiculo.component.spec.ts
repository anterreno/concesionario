import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarCondicionVehiculoComponent } from './modificar-condicion-vehiculo.component';

describe('ModificarCondicionVehiculoComponent', () => {
  let component: ModificarCondicionVehiculoComponent;
  let fixture: ComponentFixture<ModificarCondicionVehiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarCondicionVehiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarCondicionVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
