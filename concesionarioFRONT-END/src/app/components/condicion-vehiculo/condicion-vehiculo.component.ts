import { Router } from '@angular/router';
import { CondicionService } from './../../service/condicion.service';
import { Condicion } from './../../models/condicion';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicion-vehiculo',
  templateUrl: './condicion-vehiculo.component.html',
  styleUrls: ['./condicion-vehiculo.component.css']
})
export class CondicionVehiculoComponent implements OnInit {

  condiciones: Condicion[];
  cantCondiciones: number;
  ids: number[] = [];

  constructor(private _servicioCondicion: CondicionService, private _router: Router) { }

  ngOnInit(): void {
    this._servicioCondicion.get().subscribe((respuesta) => {
      this.condiciones = respuesta;  });
  }

  eliminar(id: number) {
    this.cantCondiciones = this.condiciones.length;
    for (var i = 0; i < this.cantCondiciones; i++) {
      const esta = this.ids.indexOf(this.condiciones[i].id);
      if (esta === -1) {
        this.ids.push(this.condiciones[i].id);
      }
    }
    var indice = this.ids.indexOf(id);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar la condición del vehículo?')) {
      console.log(id);
      this._servicioCondicion.eliminar(id).subscribe((respuesta) => {
       this.condiciones.splice(indice,1);
      });
      location.reload();
    }
  }

  modificar(id: number){
    this._router.navigate(['/condicion/modificar/' + id]);
  }


}
