import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MarcaService } from 'src/app/service/marca.service';

@Component({
  selector: 'app-modificar-marca',
  templateUrl: './modificar-marca.component.html',
  styleUrls: ['./modificar-marca.component.css']
})
export class ModificarMarcaComponent implements OnInit {
  marca: any = {};

  constructor( private activate: ActivatedRoute,
               private servicioMarca: MarcaService,
               private router: Router,
               private toastr: ToastrService ) { this.activate.params.subscribe(parametros => {this.servicioMarca.getMarca(parametros['idMarca']).subscribe(respuesta => {
                this.marca = respuesta;
              }); }); }

  ngOnInit(): void {
  }

  modificarMarca(miFormulario: NgForm) {
    let marca: any = {};
    let idMarca: number;
    marca.nombreMarca = miFormulario.value.nombreMarca;
    idMarca = this.marca.idMarca;
    marca.idMarca = this.marca.idMarca;
    if (miFormulario.valid) {
      this.servicioMarca.modificarMarca(marca, idMarca).subscribe(respuesta => { console.log(respuesta); });
      this.toastr.success('Modificación exitosa!', 'OK', {timeOut: 2500});
      this.router.navigate(['/marca']);
    } else {
      alert('Revise los campos');
    }
  }
}
