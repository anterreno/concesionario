import { Component, OnInit } from '@angular/core';
import { TokenService } from './../../service/token.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MarcaService } from '../../service/marca.service';
import { ToastrService } from 'ngx-toastr';
import { Marca } from '../../models/marca';

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.css']
})
export class MarcaComponent implements OnInit {

  marca: Marca[] = [];
  cantMarcas: number;
  ids: number[] = [];
  roles: string[];
  isAdmin = false;
  isLogged = false;
  isUser = false;

  constructor( private servicioMarca: MarcaService,
               private tokenService: TokenService,
               private router: Router,
               private activate: ActivatedRoute,
               private toastr: ToastrService ) { }

  ngOnInit(): void {
    this.activate.params.subscribe(parametros => {this.servicioMarca.get().subscribe(respuesta => {
      this.marca = respuesta;
    }); });
  }

  eliminarMarca(idMarca: number) {
    this.cantMarcas = this.marca.length;
    for (var i = 0; i < this.cantMarcas; i++) {
      const esta = this.ids.indexOf(this.marca[i].idMarca);
      if (esta === -1) {
        this.ids.push(this.marca[i].idMarca);
      }
    }
    var indice = this.ids.indexOf(idMarca);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar la marca?')) {
      console.log(idMarca);
      this.servicioMarca.eliminarMarca(idMarca).subscribe((respuesta) => {
       this.marca.splice(indice, 1);
      });
      location.reload();
    }
  }

  modificarMarca(idMarca: number) {
    this.router.navigate(['/marca/modificar-marca/' + idMarca]);
  }

  recargar() {
    window.location.reload();
  }

}
