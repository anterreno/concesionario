import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MarcaService } from 'src/app/service/marca.service';
import { TokenService } from 'src/app/service/token.service';

@Component({
  selector: 'app-nueva-marca',
  templateUrl: './nueva-marca.component.html',
  styleUrls: ['./nueva-marca.component.css']
})
export class NuevaMarcaComponent implements OnInit {

  constructor( private _servicio: MarcaService,
               private _router: Router,
               private _tokenService: TokenService,
               private _toastr: ToastrService ) { }

  ngOnInit(): void {
  }

  guardarMarca(miFormulario: NgForm) {
    let marca: any = {};
    marca.nombreMarca = miFormulario.value.nombreMarca;

    if (miFormulario.valid) {
      this._servicio.guardarMarca(marca).subscribe((respuesta) => {
        console.log(respuesta);
      });
      console.log(marca);
      this._toastr.info('Datos guardados con éxito!', 'OK', {
        timeOut: 2500,
        positionClass: 'toast-top-center'
      });
      this._router.navigate(['/marca']);
    } else {
      alert('Revise los campos');
    }
  }
}
