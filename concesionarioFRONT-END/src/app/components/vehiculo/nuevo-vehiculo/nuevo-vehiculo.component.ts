import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TipoVehiculoService } from './../../../service/tipo-vehiculo.service';
import { TipoCombustibleService } from './../../../service/tipo-combustible.service';
import { MotorService } from './../../../service/motor.service';
import { CondicionService } from './../../../service/condicion.service';
import { ColoresService } from './../../../service/colores.service';
import { VersionService } from './../../../service/version.service';
import { ModeloService } from './../../../service/modelo.service';
import { MarcaService } from './../../../service/marca.service';
import { VehiculoService } from './../../../service/vehiculo.service';
import { Vehiculo } from './../../../models/vehiculo';
import { TipoVehiculo } from './../../../models/tipo-vehiculo';
import { TipoCombustible } from './../../../models/tipo-combustible';
import { Motor } from './../../../models/motor';
import { Condicion } from './../../../models/condicion';
import { Color } from './../../../models/color';
import { Version } from './../../../models/version';
import { Modelo } from 'src/app/models/modelo';
import { Marca } from './../../../models/marca';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nuevo-vehiculo',
  templateUrl: './nuevo-vehiculo.component.html',
  styleUrls: ['./nuevo-vehiculo.component.css'],
})
export class NuevoVehiculoComponent implements OnInit {
  vehiculo = new Vehiculo();
  marcas: Marca[] = [];
  modelos: Modelo[] = [];
  versiones: Version[] = [];
  colores: Color[] = [];
  condiciones: Condicion[] = [];
  motores: Motor[] = [];
  tipoCombustibles: TipoCombustible[] = [];
  tipoVehiculos: TipoVehiculo[] = [];
  marcaElegida: Marca = null;
  constructor(
    private _servicioVehiculo: VehiculoService,
    private _servicioMarca: MarcaService,
    private _servicioModelo: ModeloService,
    private _servicioVersion: VersionService,
    private _servicioColor: ColoresService,
    private _servicioCondicion: CondicionService,
    private _servicioMotores: MotorService,
    private _servicioTipoCombustible: TipoCombustibleService,
    private _servicioTipoVehiculo: TipoVehiculoService,
    private _router: Router,
    private _toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this._servicioMarca.get().subscribe((respuesta) => {
      this.marcas = respuesta;
    });
    this._servicioModelo.get().subscribe((respuesta) => {
      this.modelos = respuesta;
    });
    this._servicioVersion.get().subscribe((respuesta) => {
      this.versiones = respuesta;
    });
    this._servicioColor.get().subscribe((respuesta) => {
      this.colores = respuesta;
    });
    this._servicioCondicion.get().subscribe((respuesta) => {
      this.condiciones = respuesta;
    });
    this._servicioMotores.get().subscribe((respuesta) => {
      this.motores = respuesta;
    });
    this._servicioTipoCombustible.get().subscribe((respuesta) => {
      this.tipoCombustibles = respuesta;
    });
    this._servicioTipoVehiculo.get().subscribe((respuesta) => {
      this.tipoVehiculos = respuesta;
    });
  }

  guardar(formulario) {
    if (formulario.valid) {
      this._servicioVehiculo.guardar(this.vehiculo).subscribe((respuesta) => {
        console.log(respuesta);
      });
      this._toastr.success('Registo guardado exitosamente!');
    }
    this._router.navigate(['/vehiculo']);
  }
}
