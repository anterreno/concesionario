import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { VehiculoService } from './../../service/vehiculo.service';
import { Vehiculo } from './../../models/vehiculo';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {
  vehiculos: Vehiculo[];
  cantVehiculos: number;
  ids: number[] = [];
  constructor(private _servicioVehiculo: VehiculoService,
    private _router: Router,
    private _toastr: ToastrService) { }

  ngOnInit(): void {
    this._servicioVehiculo.get().subscribe((respuesta) => {
      this.vehiculos = respuesta;
    });
  }

  eliminar(id: number) {
    this.cantVehiculos = this.vehiculos.length;
    for (var i = 0; i < this.cantVehiculos; i++) {
      const esta = this.ids.indexOf(this.vehiculos[i].id);
      if (esta === -1) {
        this.ids.push(this.vehiculos[i].id);
      }
    }
    var indice = this.ids.indexOf(id);
    console.log(this.ids);
    console.log(indice);
    if (confirm('¿Está seguro que desea eliminar el vehículo?')) {
      console.log(id);
      this._servicioVehiculo.eliminar(id).subscribe((respuesta) => {
       this.vehiculos.splice(indice,1);

      });
      location.reload();
      // this._toastr.error('Registo eliminado exitosamente!');
    }
  }

  modificar(id: number) {
    this._router.navigate(['/vehiculo/modificar/' + id]);
  }

}
