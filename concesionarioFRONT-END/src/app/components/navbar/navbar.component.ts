import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/service/token.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLogged = false;
  roles: string[];
  isAdmin = false;
  isUser = false;
  usuarioLog: String;

  constructor(private tokenService: TokenService, private router: Router) { }

  ngOnInit() {
    this.roles = this.tokenService.getAuthorities();
    this.usuarioLog = this.tokenService.getUserName();
    console.log("usuario log: ", this.usuarioLog);
    this.roles.forEach(rol => {
      if (rol === "Administrador") {
        this.isAdmin = true;
      }
    });

    if (this.tokenService.getToken()) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
    }
  }
  onLogOut(): void {
    this.tokenService.logOut();
    this.router.navigate(['/login']);
  }

}