import { Component, OnInit } from '@angular/core';
import{UrlService} from 'src/app/service/url.service'
import { Router } from '@angular/router';
import {LoginUsuario} from 'src/app/models/login-usuario'
import {TokenService} from 'src/app/service/token.service'
import {LoginService} from 'src/app/service/login.service'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLogged = false;
  isLoginFail = false;
  loginUsuario: LoginUsuario;
  nombreUsuario: string;
  contra: string;
  roles: string[] =[];
  errMsj: string;

  constructor(
    private tokenService : TokenService,
    private urlService: UrlService,
    private loginService : LoginService,
    private router: Router,
    private toastr : ToastrService
  ) { }

  ngOnInit() {
    if(this.tokenService.getToken())
    this.isLogged = true;
    this.isLoginFail = false
    this.roles = this.tokenService.getAuthorities();
  }

  onLogin(): void{
    this.loginUsuario = new LoginUsuario(this.nombreUsuario, this.contra);
    this.loginService.login(this.loginUsuario).subscribe(
      data => {
        this.isLogged = true;
        this.isLoginFail = false;

        this.tokenService.setToken(data.token);
        this.tokenService.setUserName(data.nombreUsuario);
        this.tokenService.setAuthorities(data.authorities);
        this.tokenService.setId(data.id);
        this.roles = data.authorities;
        this.router.navigate(['/home']);
        this.toastr.success('Bienvenido!' , 'OK', { timeOut: 2500, positionClass: 'toast-top-center' });
        this.router.navigate(['/home']);
      },
      err => {
        this.isLogged = false;
        this.isLoginFail = true;
        this.toastr.error('Usuario y/o Contraseña Incorrectos' , 'Fallo al iniciar sesión', { timeOut: 2500, positionClass: 'toast-top-center' });

      }
    )
  }

}
