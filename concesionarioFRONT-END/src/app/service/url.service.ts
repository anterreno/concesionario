import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

// esto es lo mismo que auth.service de el
export class UrlService {

  private URL: string = "http://localhost:8080";

  constructor(private httpClient : HttpClient ) { }

  getURLBase(): string {
    return this.URL;
  }


}
