import { Observable } from 'rxjs';
import { UrlService } from './url.service';
import { HttpClient } from '@angular/common/http';
import { TipoVehiculo } from './../models/tipo-vehiculo';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TipoVehiculoService {

  private vehiculo: TipoVehiculo[] = [];

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getVersiones(): TipoVehiculo[]{
    return this.vehiculo;
  }

  getVersion(id: number): Observable<TipoVehiculo[]>{
    return this._http.get<TipoVehiculo[]>(this._url.getURLBase() + '/auth/tipovehiculo/' + id);
  }

  get(): Observable<TipoVehiculo[]> {
    return this._http.get<TipoVehiculo[]>(this._url.getURLBase() + '/auth/tipovehiculo');
  }

  guardar(vehiculo: TipoVehiculo): Observable<TipoVehiculo>{
    return this._http.post<TipoVehiculo>(this._url.getURLBase() + '/auth/tipovehiculo', vehiculo);
  }

  eliminar(id: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/tipovehiculo/' + id);
  }

  modificar(vehiculo: TipoVehiculo, id: number): Observable<TipoVehiculo> {
    return this._http.put<TipoVehiculo>(this._url.getURLBase() + '/auth/tipovehiculo' + id, vehiculo);

  }
}
