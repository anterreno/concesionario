import { Observable } from 'rxjs';
import { UrlService } from './url.service';
import { HttpClient } from '@angular/common/http';
import { Vehiculo } from './../models/vehiculo';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  private vehiculo: Vehiculo[] = [];

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getVersiones(): Vehiculo[]{
    return this.vehiculo;
  }

  getVersion(id: number): Observable<Vehiculo[]>{
    return this._http.get<Vehiculo[]>(this._url.getURLBase() + '/auth/vehiculo/' + id);
  }

  get(): Observable<Vehiculo[]> {
    return this._http.get<Vehiculo[]>(this._url.getURLBase() + '/auth/vehiculo');
  }

  guardar(vehiculo: Vehiculo): Observable<Vehiculo>{
    return this._http.post<Vehiculo>(this._url.getURLBase() + '/auth/vehiculo', vehiculo);
  }

  eliminar(id: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/vehiculo/' + id);
  }

  modificar(vehiculo: Vehiculo, id: number): Observable<Vehiculo> {
    return this._http.put<Vehiculo>(this._url.getURLBase() + '/auth/vehiculo' + id, vehiculo);

  }
}
