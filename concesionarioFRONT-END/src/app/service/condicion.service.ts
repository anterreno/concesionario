import { Observable } from 'rxjs';
import { UrlService } from './url.service';
import { HttpClient } from '@angular/common/http';
import { Condicion } from './../models/condicion';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CondicionService {

  private condicion: Condicion[] = [];

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getVersiones(): Condicion[]{
    return this.condicion;
  }

  getVersion(id: number): Observable<Condicion[]>{
    return this._http.get<Condicion[]>(this._url.getURLBase() + '/auth/condicion/' + id);
  }

  get(): Observable<Condicion[]> {
    return this._http.get<Condicion[]>(this._url.getURLBase() + '/auth/condicion');
  }

  guardar(condicion: Condicion): Observable<Condicion>{
    return this._http.post<Condicion>(this._url.getURLBase() + '/auth/condicion', condicion);
  }

  eliminar(id: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/condicion/' + id);
  }

  modificar(condicion: Condicion, id: number): Observable<Condicion> {
    return this._http.put<Condicion>(this._url.getURLBase() + '/auth/condicion' + id, condicion);

  }
}
