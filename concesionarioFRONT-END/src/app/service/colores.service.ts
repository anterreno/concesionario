import { Injectable } from '@angular/core';
import { Color } from '../models/color';
import { UrlService } from './url.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColoresService {
  private colores: Color[] = [];

  constructor( private _http: HttpClient, private _url: UrlService ) { }

  getColores(): Color[] {
    return this.colores;
  }

  getColor(idColor: number): Observable<any> {
    return this._http.get<Color[]>(this._url.getURLBase() + '/auth/color/' + idColor);
  }

  get(): Observable<any> {
    return this._http.get<Color[]>(this._url.getURLBase() + '/auth/color');
  }

  guardarColor(color: any): Observable<any> {
    return this._http.post(this._url.getURLBase() + '/auth/color', color);
  }

  eliminarColor(idColor: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/color/' + idColor);
  }

  modificarColor(color: any, idColor: number): Observable<any> {
    const url = this._url.getURLBase() + '/auth/color/' + idColor;
    console.log(url);
    return this._http.put<any>(this._url.getURLBase() + '/auth/color/' + idColor, color);
  }

}
