import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Modelo } from '../models/modelo';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class ModeloService {
  
  private modelo: Modelo[] = [];
  private modelos: Modelo;

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getModelo(idModelo: number): Observable<any> {
    return this._http.get<Modelo[]>(this._url.getURLBase() + '/auth/modelo/' + idModelo);
  }
  
  get(): Observable<any> {
    return this._http.get<Modelo[]>(this._url.getURLBase() + '/auth/modelo');
  }
  
  guardar(modelo: any): Observable<any>{
    return this._http.post(this._url.getURLBase() + '/auth/modelo', modelo);
  }
  
  eliminar(idModelo: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/modelo/' + idModelo);
  }
  
  modificar(modelo: any, idModelo: number): Observable<any> {
    const url = this._url.getURLBase() + '/auth/modelo/' + idModelo;
    console.log(url);
    return this._http.put<any>(this._url.getURLBase() + '/auth/modelo/' + idModelo, modelo);
  
  }
}
