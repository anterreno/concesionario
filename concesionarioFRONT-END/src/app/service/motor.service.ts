import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Motor } from '../models/motor';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class MotorService {

  private motor: Motor[] = [];
  private motores: Motor;

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getMotor(idMotor: number): Observable<any> {
    return this._http.get<Motor[]>(this._url.getURLBase() + '/auth/motor/' + idMotor);
  }
  
  get(): Observable<any> {
    return this._http.get<Motor[]>(this._url.getURLBase() + '/auth/motor');
  }
  
  guardar(motor: any): Observable<any>{
    return this._http.post(this._url.getURLBase() + '/auth/motor', motor);
  }
  
  eliminar(idMotor: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/motor/' + idMotor);
  }
  
  modificar(motor: any, idMotor: number): Observable<any> {
    const url = this._url.getURLBase() + '/auth/motor/' + idMotor;
    console.log(url);
    return this._http.put<any>(this._url.getURLBase() + '/auth/motor/' + idMotor, motor);
  
  }
}

