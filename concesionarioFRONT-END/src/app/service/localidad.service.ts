import { Observable } from 'rxjs';
import { UrlService } from './url.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalidadService {

  constructor(private _http: HttpClient, private _url: UrlService) { }

  get(): Observable<any> {
    return this._http.get<Localidad[]>(this._url.getURLBase() + '/auth/localidades');
  }
}

export interface Localidad {
  idLocalidad: number;
  nombreLocalidad: string;
}