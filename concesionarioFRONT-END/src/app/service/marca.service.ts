import { Injectable } from '@angular/core';
import { Color } from '../models/color';
import { UrlService } from './url.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Marca } from '../models/marca';

@Injectable({
  providedIn: 'root'
})
export class MarcaService {
  private marca: Marca[] = [];

  constructor( private _http: HttpClient, private _url: UrlService ) { }

  getMarcas(): Marca[] {
    return this.marca;
  }

  getMarca(idMarca: number): Observable<any> {
    return this._http.get<Marca[]>(this._url.getURLBase() + '/auth/marca/' + idMarca);
  }

  get(): Observable<any> {
    return this._http.get<Marca[]>(this._url.getURLBase() + '/auth/marca');
  }

  guardarMarca(marca: any): Observable<any> {
    return this._http.post(this._url.getURLBase() + '/auth/marca', marca);
  }

  eliminarMarca(idMarca: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/marca/' + idMarca);
  }

  modificarMarca(marca: any, idMarca: number): Observable<any> {
    const url = this._url.getURLBase() + '/auth/marca/' + idMarca;
    console.log(url);
    return this._http.put<any>(this._url.getURLBase() + '/auth/marca/' + idMarca, marca);
  }
}
