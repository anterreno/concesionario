import { HttpClient } from '@angular/common/http';
import { Version } from './../models/version';
import { Injectable } from '@angular/core';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VersionService {
  private version: Version[] = [];

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getVersiones(): Version[]{
    return this.version;
  }

  getVersion(id: number): Observable<Version[]>{
    return this._http.get<Version[]>(this._url.getURLBase() + '/auth/version/' + id);
  }

  get(): Observable<Version[]> {
    return this._http.get<Version[]>(this._url.getURLBase() + '/auth/version');
  }

  guardar(version: Version): Observable<Version>{
    return this._http.post<Version>(this._url.getURLBase() + '/auth/version', version);
  }

  eliminar(id: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/version/' + id);
  }

  modificar(version: Version, id: number): Observable<Version> {
    return this._http.put<Version>(this._url.getURLBase() + '/auth/version' + id, version);

  }
}
