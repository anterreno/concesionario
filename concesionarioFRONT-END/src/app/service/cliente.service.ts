import { UrlService } from './url.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Localidad } from '../models/localidad';

@Injectable({
  providedIn: 'root',
})
export class ClienteService {
  private clientes: Cliente[] = [];
 

  constructor(private _http: HttpClient, private _url: UrlService) {}

  getClientes(): Cliente[] {
    return this.clientes;
  }

  getCliente(idCliente: number): Observable<any> {
    return this._http.get<Cliente[]>(this._url.getURLBase() + '/auth/clientes/' + idCliente);
  }

  get(): Observable<any> {
    return this._http.get<Cliente[]>(this._url.getURLBase() + '/auth/clientes');
  }

  guardarCliente(cliente: any): Observable<any> {
    return this._http.post(this._url.getURLBase() + '/auth/clientes', cliente);
  }

  eliminarCliente(idCliente: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/clientes/' + idCliente);
  }

  modificarCliente(cliente: any, idCliente: number): Observable<any> {
    const url = this._url.getURLBase() + '/auth/clientes/' + idCliente;
    console.log(url);
    return this._http.put<any>(this._url.getURLBase() + '/auth/clientes/' + idCliente, cliente);
  }
}
export interface Cliente {
    idCliente:number;
    nombreCliente : string;
  	apellidoCliente : string;
    emailCliente: string;
    direccionCliente: string;
    dniCliente : string;
    localidad:Localidad;
    telefonoCliente: string;
}
