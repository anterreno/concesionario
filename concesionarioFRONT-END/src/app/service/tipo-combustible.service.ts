import { Observable } from 'rxjs';
import { UrlService } from './url.service';
import { HttpClient } from '@angular/common/http';
import { TipoCombustible } from './../models/tipo-combustible';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TipoCombustibleService {

  private combustible: TipoCombustible[] = [];

  constructor(private _http: HttpClient, private _url: UrlService) { }

  getVersiones(): TipoCombustible[]{
    return this.combustible;
  }

  getVersion(id: number): Observable<TipoCombustible[]>{
    return this._http.get<TipoCombustible[]>(this._url.getURLBase() + '/auth/tipocombustible/' + id);
  }

  get(): Observable<TipoCombustible[]> {
    return this._http.get<TipoCombustible[]>(this._url.getURLBase() + '/auth/tipocombustible');
  }

  guardar(combustible: TipoCombustible): Observable<TipoCombustible>{
    return this._http.post<TipoCombustible>(this._url.getURLBase() + '/auth/tipocombustible', combustible);
  }

  eliminar(id: number) {
    return this._http.delete(this._url.getURLBase() + '/auth/tipocombustible/' + id);
  }

  modificar(combustible: TipoCombustible, id: number): Observable<TipoCombustible> {
    return this._http.put<TipoCombustible>(this._url.getURLBase() + '/auth/tipocombustible' + id, combustible);

  }
}
