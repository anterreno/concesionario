import { NuevoVehiculoComponent } from './components/vehiculo/nuevo-vehiculo/nuevo-vehiculo.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NuevoUsuarioComponent } from './components/nuevo-usuario/nuevo-usuario.component';
import { ProdGuardService as guard} from '../app/guards/prod-guard.service';
import { HomeComponent } from './components/home/home.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { NuevoClienteComponent } from './components/clientes/nuevo-cliente/nuevo-cliente.component';
import { VersionComponent } from './components/version/version.component';
import { ModificarVersionComponent } from './components/version/modificar-version/modificar-version.component';
import { CondicionVehiculoComponent } from './components/condicion-vehiculo/condicion-vehiculo.component';
import { NuevaCondicionVehiculoComponent } from './components/condicion-vehiculo/nueva-condicion-vehiculo/nueva-condicion-vehiculo.component';
import { ModificarCondicionVehiculoComponent } from './components/condicion-vehiculo/modificar-condicion-vehiculo/modificar-condicion-vehiculo.component';
import { TipoCombustibleComponent } from './components/tipo-combustible/tipo-combustible.component';
import { NuevoTipoCombustibleComponent } from './components/tipo-combustible/nuevo-tipo-combustible/nuevo-tipo-combustible.component';
import { ModificarTipoCombustibleComponent } from './components/tipo-combustible/modificar-tipo-combustible/modificar-tipo-combustible.component';
import { TipoVehiculoComponent } from './components/tipo-vehiculo/tipo-vehiculo.component';
import { NuevoTipoVehiculoComponent } from './components/tipo-vehiculo/nuevo-tipo-vehiculo/nuevo-tipo-vehiculo.component';
import { ModificarTipoVehiculoComponent } from './components/tipo-vehiculo/modificar-tipo-vehiculo/modificar-tipo-vehiculo.component';
import { NuevaVersionComponent } from './components/version/nueva-version/nueva-version.component';
import { MotorComponent } from './components/motor/motor.component';
import { ModeloComponent } from './components/modelo/modelo.component';
import { NuevoMotorComponent } from './components/motor/nuevo-motor/nuevo-motor.component';
import { NuevoModeloComponent } from './components/modelo/nuevo-modelo/nuevo-modelo.component';
import { ModificarMotorComponent } from './components/motor/modificar-motor/modificar-motor.component';
import { ModificarModeloComponent } from './components/modelo/modificar-modelo/modificar-modelo.component';
import { ColoresComponent } from './components/colores/colores.component';
import { NuevoColorComponent } from './components/colores/nuevo-color/nuevo-color.component';
import { ModificarColorComponent } from './components/colores/modificar-color/modificar-color.component';
import { MarcaComponent } from './components/marca/marca.component';
import { NuevaMarcaComponent } from './components/marca/nueva-marca/nueva-marca.component';
import { ModificarMarcaComponent } from './components/marca/modificar-marca/modificar-marca.component';
const routes: Routes = [
  {path: 'login', component:LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'clientes', component: ClientesComponent},
  {path: 'auth/nuevo', component: NuevoUsuarioComponent, canActivate: [guard], data: { expectedRol: ['Administrador'] }},
  {path: 'clientes/nuevo', component: NuevoClienteComponent},
  {path: 'version', component: VersionComponent},
  {path: 'version/nueva', component: NuevaVersionComponent},
  {path: 'version/modificar/:id', component: ModificarVersionComponent},
  {path: 'condicion', component: CondicionVehiculoComponent},
  {path: 'condicion/nueva', component: NuevaCondicionVehiculoComponent},
  {path: 'condicion/modificar/:id', component: ModificarCondicionVehiculoComponent},
  {path: 'tipocombustible', component: TipoCombustibleComponent},
  {path: 'tipocombustible/nuevo', component: NuevoTipoCombustibleComponent},
  {path: 'tipocombustible/modificar/:id', component: ModificarTipoCombustibleComponent},
  {path: 'tipovehiculo', component: TipoVehiculoComponent},
  {path: 'tipovehiculo/nuevo', component: NuevoTipoVehiculoComponent},
  {path: 'tipovehiculo/modificar/:id', component: ModificarTipoVehiculoComponent},
  {path: 'colores', component: ColoresComponent },
  {path: 'colores/nuevo-color', component: NuevoColorComponent },
  {path: 'colores/modificar-color/:idColor', component: ModificarColorComponent},
  {path: 'marca', component: MarcaComponent },
  {path: 'marca/nueva-marca', component: NuevaMarcaComponent },
  {path: 'marca/modificar-marca/:idMarca', component: ModificarMarcaComponent },
  {path: 'motor', component: MotorComponent},
  {path: 'modelo', component: ModeloComponent},
  {path: 'motor/nuevo', component: NuevoMotorComponent},
  {path: 'modelo/nuevo', component: NuevoModeloComponent},
  {path: 'motor/modificar/:id', component: ModificarMotorComponent},
  {path: 'modelo/modificar/:id', component: ModificarModeloComponent},
  {path: 'vehiculo', component: VehiculoComponent},
  {path: 'vehiculo/nuevo', component: NuevoVehiculoComponent},
  // {path: 'vehiculo/modificar/:id', component: ModificarVehiculoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
