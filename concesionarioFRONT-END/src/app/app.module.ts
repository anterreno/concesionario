import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NuevoUsuarioComponent } from './components/nuevo-usuario/nuevo-usuario.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { NuevoClienteComponent } from './components/clientes/nuevo-cliente/nuevo-cliente.component';
import { ModificarVersionComponent } from './components/version/modificar-version/modificar-version.component';
import { VersionComponent } from './components/version/version.component';
import { NuevaVersionComponent } from './components/version/nueva-version/nueva-version.component';
import { CondicionVehiculoComponent } from './components/condicion-vehiculo/condicion-vehiculo.component';
import { NuevaCondicionVehiculoComponent } from './components/condicion-vehiculo/nueva-condicion-vehiculo/nueva-condicion-vehiculo.component';
import { ModificarCondicionVehiculoComponent } from './components/condicion-vehiculo/modificar-condicion-vehiculo/modificar-condicion-vehiculo.component';
import { TipoCombustibleComponent } from './components/tipo-combustible/tipo-combustible.component';
import { NuevoTipoCombustibleComponent } from './components/tipo-combustible/nuevo-tipo-combustible/nuevo-tipo-combustible.component';
import { ModificarTipoCombustibleComponent } from './components/tipo-combustible/modificar-tipo-combustible/modificar-tipo-combustible.component';
import { TipoVehiculoComponent } from './components/tipo-vehiculo/tipo-vehiculo.component';
import { NuevoTipoVehiculoComponent } from './components/tipo-vehiculo/nuevo-tipo-vehiculo/nuevo-tipo-vehiculo.component';
import { ModificarTipoVehiculoComponent } from './components/tipo-vehiculo/modificar-tipo-vehiculo/modificar-tipo-vehiculo.component';
import { NuevoModeloComponent } from './components/modelo/nuevo-modelo/nuevo-modelo.component';
import { ModificarModeloComponent } from './components/modelo/modificar-modelo/modificar-modelo.component';
import { ModificarMotorComponent } from './components/motor/modificar-motor/modificar-motor.component';
import { NuevoMotorComponent } from './components/motor/nuevo-motor/nuevo-motor.component';
import { MotorComponent } from './components/motor/motor.component';
import { ModeloComponent } from './components/modelo/modelo.component';
import { ColoresComponent } from './components/colores/colores.component';
import { ModificarColorComponent } from './components/colores/modificar-color/modificar-color.component';
import { ModificarMarcaComponent } from './components/marca/modificar-marca/modificar-marca.component';
import { MarcaComponent } from './components/marca/marca.component';
import { NuevaMarcaComponent } from './components/marca/nueva-marca/nueva-marca.component';
import { NuevoColorComponent } from './components/colores/nuevo-color/nuevo-color.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { NuevoVehiculoComponent } from './components/vehiculo/nuevo-vehiculo/nuevo-vehiculo.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ColoresComponent,
    ModificarColorComponent,
    ModificarMarcaComponent,
    MarcaComponent,
    NuevoColorComponent,
    NuevaMarcaComponent,
    NavbarComponent,
    NuevoUsuarioComponent,
    HomeComponent,
    ClientesComponent,
    NuevoClienteComponent,
    VersionComponent,
    NuevaVersionComponent,
    ModificarVersionComponent,
    NuevaVersionComponent,
    CondicionVehiculoComponent,
    NuevaCondicionVehiculoComponent,
    ModificarCondicionVehiculoComponent,
    TipoCombustibleComponent,
    NuevoTipoCombustibleComponent,
    ModificarTipoCombustibleComponent,
    TipoVehiculoComponent,
    NuevoTipoVehiculoComponent,
    ModificarTipoVehiculoComponent,
    MotorComponent,
    ModeloComponent,
    NuevoMotorComponent,
    NuevoModeloComponent,
    ModificarModeloComponent,
    ModificarMotorComponent,
    VehiculoComponent,
    NuevoVehiculoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot({
      timeOut: 1500,
      progressAnimation: 'increasing',
      progressBar: true,
      positionClass: 'toast-top-center',
      preventDuplicates: true
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
