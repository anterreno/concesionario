

import { Rol } from '../service/roles.service';
import { Localidad } from './localidad';

export class Cliente {
    idCliente:number;
    nombreCliente : string;
  	apellidoCliente : string;
    emailCliente: string;
    direccionCliente: string;
    dniCliente : string;
    localidad:Localidad;
    telefonoCliente: string;
}