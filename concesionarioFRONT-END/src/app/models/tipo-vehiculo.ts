import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';

export class TipoVehiculo {
  id: number;
  nombre: string;
}
