import { Modelo } from './modelo';
export class Marca {
    idMarca: number;
    nombreMarca: string;
    modelo: Modelo[];
}
