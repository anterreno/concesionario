import { Motor } from './motor';
import { Modelo } from './modelo';
import { Marca } from './marca';
import { Condicion } from './condicion';
import { Color } from './color';
import { Version } from './version';
import { TipoVehiculo } from './tipo-vehiculo';
import { TipoCombustible } from './tipo-combustible';

export class Vehiculo {
  id: number;
  marca: Marca;
  modelo: Modelo;
  version: Version;
  color: Color;
  condicion: Condicion;
  motor: Motor;
  tipoCombustible: TipoCombustible;
  tipoVehiculo: TipoVehiculo;
  descripcion: string;
  kms: number;
  precioLista: DoubleRange;
  stock: number;
}
