import { Modelo } from 'src/app/models/modelo';
export class Version {
  id: number;
  nombre: string;
  observacion: string;
  modelo: Modelo;
}
