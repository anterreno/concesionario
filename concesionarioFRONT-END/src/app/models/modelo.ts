import { Version } from './version';
import { Marca } from './marca';
export class Modelo {
    idModelo: number;
    nombre: string;
    marca: Marca;
    version: Version[];
  }
