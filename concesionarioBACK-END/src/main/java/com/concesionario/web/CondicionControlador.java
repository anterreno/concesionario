package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.CondicionServicio;
import com.concesionario.logica.VersionServicio;
import com.concesionario.modelo.Condicion;
import com.concesionario.modelo.Version;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/condicion")
public class CondicionControlador {
	@Autowired
	private CondicionServicio condicionServicio;
	
	@GetMapping
	public List<Condicion> listarTodas(){
		return condicionServicio.listarTodas();
	}
	
	@GetMapping(value="/{id}")
	public Condicion getCondicion(@PathVariable(name="id") Integer id) {
		return condicionServicio.getCondicion(id);
	}
	
	@PostMapping
	public Condicion guardar(@RequestBody Condicion c) {
		return condicionServicio.guardar(c);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public Condicion actualizar(@RequestBody Condicion c, @PathVariable(name="id") Integer id) {
		return condicionServicio.actualizar(c);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		condicionServicio.eliminar(id);
	}
}
