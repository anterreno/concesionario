package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.ModeloServicio;
import com.concesionario.modelo.Modelo;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RestController
@RequestMapping("auth/modelo")
public class ModeloControlador {
	@Autowired
	private ModeloServicio servicio;
	
	@GetMapping
	public List<Modelo> listarTodos(){
		return servicio.listarTodos();
	}
	
	@GetMapping(value= "/{idModelo}")
	public Modelo getModelo(@PathVariable(name = "idModelo") Integer idModelo) {
		return servicio.getModelo(idModelo);
	}
	
//	@RequestMapping(params= {"idVehiculo"})
//    public Iterable<Modelo> getModeloPorVehiculoId(@RequestParam(name="idVehiculo")Integer idVehiculo){
//        return servicio.getModeloPorVehiculoId(idVehiculo);
//    }
	
	@PostMapping
	public Modelo guardar(@RequestBody Modelo m) {
		return servicio.guardar(m);
	}
	
	@RequestMapping(value = "/{idModelo}", method = RequestMethod.PUT)
	public Modelo actualizar(@RequestBody Modelo m, @PathVariable(name = "idModelo") Integer idModelo ) {
		return servicio.actualizar(m);
	}
	
	@RequestMapping(value="/{idModelo}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="idModelo") Integer idModelo) {
		servicio.eliminar(idModelo);
	}

}
