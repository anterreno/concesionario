package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.MarcaServicio;
import com.concesionario.modelo.Marca;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/marca")
public class MarcaControlador {
	
	@Autowired
	private MarcaServicio servicio;
	
	@GetMapping
	public List<Marca> listarTodas(){
		return servicio.listarTodas();
	}

	@GetMapping(value="/{idMarca}")
	public Marca getMarca(@PathVariable(name="idMarca") Integer idMarca) {
		return servicio.getMarca(idMarca);
	}
	
	@PostMapping
	public Marca guardar(@RequestBody Marca m) {
		if(servicio.existsByNombreMarca(m.getNombreMarca())){
			throw new RuntimeException("Este nombre de marca ya existe");	
		}
		return servicio.guardar(m);
	}
	
	@RequestMapping(value="/{idMarca}", method=RequestMethod.PUT)
	public Marca actualizar(@RequestBody Marca m, @PathVariable(name="idMarca") Integer idMarca) {
		return servicio.actualizar(m);
	}
	
	@RequestMapping(value="/{idMarca}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="idMarca") Integer idMarca) {
		servicio.eliminar(idMarca);
	}

}
