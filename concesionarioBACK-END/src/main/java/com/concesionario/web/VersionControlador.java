package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.VersionServicio;
import com.concesionario.modelo.Version;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/version")
public class VersionControlador {
	@Autowired
	private VersionServicio versionServicio;
	
	@GetMapping
	public List<Version> listarTodas(){
		return versionServicio.listarTodas();
	}
	
	@GetMapping(value="/{id}")
	public Version getVersion(@PathVariable(name="id") Integer id) {
		return versionServicio.getVersion(id);
	}
	
	@PostMapping
	public Version guardar(@RequestBody Version v) {
		return versionServicio.guardar(v);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public Version actualizar(@RequestBody Version v, @PathVariable(name="id") Integer id) {
		return versionServicio.actualizar(v);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		versionServicio.eliminar(id);
	}
}
