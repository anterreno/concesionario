package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.TipoCombustibleServicio;
import com.concesionario.logica.TipoVehiculoServicio;
import com.concesionario.modelo.TipoCombustible;
import com.concesionario.modelo.TipoVehiculo;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/tipovehiculo")
public class TipoVehiculoControlador {
	@Autowired
	private TipoVehiculoServicio tipoVehiculoServicio;
	
	@GetMapping
	public List<TipoVehiculo> listarTodas(){
		return tipoVehiculoServicio.listarTodas();
	}
	
	@GetMapping(value="/{id}")
	public TipoVehiculo getTipoVehiculo(@PathVariable(name="id") Integer id) {
		return tipoVehiculoServicio.getTipoVehiculo(id);
	}
	
	@PostMapping
	public TipoVehiculo guardar(@RequestBody TipoVehiculo tv) {
		return tipoVehiculoServicio.guardar(tv);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public TipoVehiculo actualizar(@RequestBody TipoVehiculo tv, @PathVariable(name="id") Integer id) {
		return tipoVehiculoServicio.actualizar(tv);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		tipoVehiculoServicio.eliminar(id);
	}
}
