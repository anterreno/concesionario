package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.CondicionServicio;
import com.concesionario.logica.TipoCombustibleServicio;
import com.concesionario.modelo.Condicion;
import com.concesionario.modelo.TipoCombustible;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/tipocombustible")
public class TipoCombustibleControlador {
	@Autowired
	private TipoCombustibleServicio tipoCombustibleSerivcio;
	
	@GetMapping
	public List<TipoCombustible> listarTodas(){
		return tipoCombustibleSerivcio.listarTodas();
	}
	
	@GetMapping(value="/{id}")
	public TipoCombustible getTipoCombustible(@PathVariable(name="id") Integer id) {
		return tipoCombustibleSerivcio.getTipoCombustible(id);
	}
	
	@PostMapping
	public TipoCombustible guardar(@RequestBody TipoCombustible tc) {
		return tipoCombustibleSerivcio.guardar(tc);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public TipoCombustible actualizar(@RequestBody TipoCombustible tc, @PathVariable(name="id") Integer id) {
		return tipoCombustibleSerivcio.actualizar(tc);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		tipoCombustibleSerivcio.eliminar(id);
	}

}
