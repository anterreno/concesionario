package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.LocalidadServicio;
import com.concesionario.modelo.Localidad;


@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/localidades")
@RestController
public class LocalidadControlador {
	@Autowired
	private LocalidadServicio servicio;
	@GetMapping
	public List<Localidad> listarTodas(){
		return servicio.listarTodas();
	}
}


