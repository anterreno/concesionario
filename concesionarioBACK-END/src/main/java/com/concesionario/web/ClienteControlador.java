package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.ClienteServicio;
import com.concesionario.modelo.Cliente;




@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RestController
@RequestMapping("auth/clientes")

public class ClienteControlador {
	@Autowired
	private ClienteServicio clienteServicio;
	
	@GetMapping
	public List<Cliente> listarTodos(){
		return clienteServicio.listarTodos();
	}
	
	@GetMapping(value= "/{idCliente}")
	public Cliente getCliente(@PathVariable(name = "idCliente") Integer idCliente) {
		return clienteServicio.getCliente(idCliente);
	}
	
	@PostMapping
	public Cliente guardar(@RequestBody Cliente c) {
		if(clienteServicio.existsByDniCliente(c.getDniCliente())){
			throw new RuntimeException("Ya existe un cliente registrado con el DNI ingresado");	
		}
		return clienteServicio.guardar(c);
	}
	
	@RequestMapping(value="/{idCliente}", method=RequestMethod.PUT)
	public Cliente actualizar(@RequestBody Cliente c, @PathVariable(name="idCliente") Integer idCliente) {
		return clienteServicio.actualizar(c);
	}
	
	@RequestMapping(value="/{idCliente}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="idCliente") Integer idCliente) {
		clienteServicio.eliminar(idCliente);
	}
}
