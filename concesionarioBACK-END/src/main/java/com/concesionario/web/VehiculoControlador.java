package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.VehiculoServicio;
import com.concesionario.modelo.Vehiculo;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/vehiculo")
public class VehiculoControlador {
	@Autowired
	private VehiculoServicio vehiculoSerivcio;
	
	@GetMapping
	public List<Vehiculo> listarTodas(){
		return vehiculoSerivcio.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Vehiculo getVehiculo(@PathVariable(name="id") Integer id) {
		return vehiculoSerivcio.getVehiculo(id);
	}
	
	@PostMapping
	public Vehiculo guardar(@RequestBody Vehiculo v) {
		return vehiculoSerivcio.guardar(v);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public Vehiculo actualizar(@RequestBody Vehiculo v, @PathVariable(name="id") Integer id) {
		return vehiculoSerivcio.actualizar(v);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		vehiculoSerivcio.eliminar(id);
	}

}
