package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.ColorServicio;
import com.concesionario.modelo.Color;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,  RequestMethod.PUT })
@RequestMapping("auth/color")
public class ColorControlador {
	@Autowired
	private ColorServicio servicio;
	
	@GetMapping
	public List<Color> listarTodos(){
		return servicio.listarTodos();
	}

	@GetMapping(value="/{idColor}")
	public Color getColor(@PathVariable(name="idColor") Integer idColor) {
		return servicio.getColor(idColor);
	}
	
	@PostMapping
	public Color guardar(@RequestBody Color c) {
		if(servicio.existsByNombreColor(c.getNombreColor())){
			throw new RuntimeException("Este nombre de color ya existe");	
		}
		return servicio.guardar(c);
	}
	
	@RequestMapping(value="/{idColor}", method=RequestMethod.PUT)
	public Color actualizar(@RequestBody Color c, @PathVariable(name="idColor") Integer idColor) {
		return servicio.actualizar(c);
	}
	
	@RequestMapping(value="/{idColor}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="idColor") Integer idColor) {
		servicio.eliminar(idColor);
	}
}
