package com.concesionario.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.logica.MotorServicio;
import com.concesionario.modelo.Motor;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RestController
@RequestMapping("auth/motor")
public class MotorControlador {
	@Autowired
	private MotorServicio servicio;
	
	@GetMapping
	public List<Motor> listarTodos(){
		return servicio.listarTodos();
	}
	
	@GetMapping(value= "/{id}")
	public Motor getMotor(@PathVariable(name = "id") Integer id) {
		return servicio.getMotor(id);
	}
	
//	@RequestMapping(params= {"idVehiculo"})
//    public Iterable<Motor> getMotorPorVehiculoId(@RequestParam(name="idVehiculo")Integer idVehiculo){
//        return servicio.getMotorPorVehiculoId(idVehiculo);
//    }
	@PostMapping
	public Motor guardar(@RequestBody Motor m) {
		return servicio.guardar(m);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Motor actualizar(@RequestBody Motor m, @PathVariable(name = "id") Integer id ) {
		return servicio.actualizar(m);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		servicio.eliminar(id);
	}

}
