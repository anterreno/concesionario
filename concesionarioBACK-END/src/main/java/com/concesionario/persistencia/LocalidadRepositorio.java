package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Localidad;

public interface LocalidadRepositorio extends JpaRepository<Localidad, Integer> {

}
