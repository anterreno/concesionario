package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Version;

public interface VersionRepositorio extends JpaRepository<Version, Integer> {

}
