package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.TipoCombustible;

public interface TipoCombustibleRepositorio extends JpaRepository<TipoCombustible, Integer> {

}
