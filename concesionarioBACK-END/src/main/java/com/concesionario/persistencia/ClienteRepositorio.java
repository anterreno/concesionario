package com.concesionario.persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.concesionario.modelo.Cliente;



@Repository
public interface ClienteRepositorio extends JpaRepository<Cliente , Integer>{
	boolean existsByDniCliente(Integer dniCliente);
	List<Cliente> findByIdCliente(Integer idCliente);
	List<Cliente> findByNombreCliente(String nombreCliente);
}
