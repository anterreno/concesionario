package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Modelo;


public interface ModeloRepositorio extends JpaRepository<Modelo, Integer> {
	
	boolean existsByNombre(String nombre);

}
