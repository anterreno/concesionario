package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.TipoVehiculo;

public interface TipoVehiculoRepositorio extends JpaRepository<TipoVehiculo, Integer> {

}
