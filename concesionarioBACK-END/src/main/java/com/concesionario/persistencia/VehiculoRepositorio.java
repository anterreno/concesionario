package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Vehiculo;

public interface VehiculoRepositorio extends JpaRepository<Vehiculo, Integer> {

}
