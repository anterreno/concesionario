package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Condicion;

public interface CondicionRepositorio extends JpaRepository<Condicion, Integer> {

}
