package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Motor;

public interface MotorRepositorio extends JpaRepository<Motor, Integer> {
	
//	boolean existsByNombreMotor(String nombreMotor);

}
