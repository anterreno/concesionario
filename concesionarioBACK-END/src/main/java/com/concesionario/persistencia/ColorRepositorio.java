package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Color;

public interface ColorRepositorio extends JpaRepository<Color, Integer> {
	boolean existsByNombreColor(String nombreColor);

//	Iterable<Color> findByVehiculo_IdVehiculo(Integer idVehiculo);

}
