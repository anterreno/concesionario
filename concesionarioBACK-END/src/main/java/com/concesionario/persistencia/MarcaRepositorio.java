package com.concesionario.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import com.concesionario.modelo.Marca;

public interface MarcaRepositorio extends JpaRepository<Marca, Integer> {
	boolean existsByNombreMarca(String nombreMarca);

}
