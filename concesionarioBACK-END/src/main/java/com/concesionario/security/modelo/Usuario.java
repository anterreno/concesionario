package com.concesionario.security.modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.sun.istack.NotNull;

import lombok.Data;



@Data
@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;
	
	
	@Column(unique = true)
	private String nombreUsuario;
	
	@NotNull
	@Column
	private String apellidoUsuario;
	
	@NotNull
	@Column
	private String email;
	
	@NotNull
	@Column
	private String nombrePila;
	
	@NotNull
	@Column
	private String contra;
	
	@NotNull
	@Column
	private String telefonoUsuario;
	

	@NotNull
	@Column
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
	private List<Rol> roles = new ArrayList<Rol>();
	
	
	
	
//	@OneToMany
//	private List<Reserva> reserva;
//	
//	
	

	public Usuario() {
		
	}




	public Usuario(String nombreUsuario, String apellidoUsuario, String email, String nombrePila, String contra,
			String telefonoUsuario) {
		super();
		this.nombreUsuario = nombreUsuario;
		this.apellidoUsuario = apellidoUsuario;
		this.email = email;
		this.nombrePila = nombrePila;
		this.contra = contra;
		this.telefonoUsuario = telefonoUsuario;
		
	}



	}


	
	

	


