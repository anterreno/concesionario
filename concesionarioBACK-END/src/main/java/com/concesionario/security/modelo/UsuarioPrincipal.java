package com.concesionario.security.modelo;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sun.istack.NotNull;

import lombok.Data;

@Data
public class UsuarioPrincipal implements UserDetails{
	
	
	private String nombreUsuario;
	private String apellidoUsuario;
	private String email;
	private String nombrePila;
	private String contra;
	private String telefonoUsuario;
	private int id;
	private Collection<? extends GrantedAuthority> authorities;
	
	

	public UsuarioPrincipal(int id, String nombreUsuario, String apellidoUsuario, String email,
			String nombrePila, String contra,String telefonoUsuario, Collection<? extends GrantedAuthority> authorities) {
		super();
		this.nombreUsuario = nombreUsuario;
		this.apellidoUsuario = apellidoUsuario;
		this.email = email;
		this.nombrePila = nombrePila;
		this.contra = contra;
		this.telefonoUsuario = telefonoUsuario;
		this.authorities = authorities;
		this.id = id;
		
	}

	public static UsuarioPrincipal build(Usuario usuario) {
		List<GrantedAuthority> authorities = 
				usuario.getRoles().stream().map(rol -> new SimpleGrantedAuthority(rol
						.getRolNombre())).collect(Collectors.toList());
		return new UsuarioPrincipal(usuario.getIdUsuario(),usuario.getNombreUsuario(), usuario.getApellidoUsuario(), usuario.getEmail(),usuario.getNombrePila(),usuario.getContra(),usuario.getTelefonoUsuario(),authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return contra;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return nombreUsuario;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public String getApellidoUsuarioComplejo() {
		return apellidoUsuario;
	}


	public String getEmailComplejo() {
		return email;
	}


	public String getNombrePilaComplejo() {
		return nombrePila;
	}

	public String getTelefonoUsuario() {
		return telefonoUsuario;
	}
	


	
	
	

}
