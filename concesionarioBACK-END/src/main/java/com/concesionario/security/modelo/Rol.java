package com.concesionario.security.modelo;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;


import lombok.Data;

@Data
@Entity
public class Rol {
	@Id
	@GeneratedValue
	private int idRol;
	
	@NotNull
	private String rolNombre;
	
//	@ManyToMany(mappedBy = "roles")
//	private UsuarioComplejo usuarioComplejo;
	
	public Rol() {
		
	}
	
	

	public Rol(@NotNull String rolNombre) {

		this.rolNombre = rolNombre;
	}

	

	
	
	
	
	
}
