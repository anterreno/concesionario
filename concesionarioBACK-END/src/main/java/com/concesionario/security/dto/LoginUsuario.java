package com.concesionario.security.dto;


import javax.validation.constraints.NotBlank;



import lombok.Data;

@Data
public class LoginUsuario {
	@NotBlank
	private String nombreUsuario;
	
	@NotBlank
	private String contra;
}
