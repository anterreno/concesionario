package com.concesionario.security.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;

import com.concesionario.security.modelo.Rol;
import lombok.Data;

@Data
public class NuevoUsuario {


	@NotBlank
	private String nombreUsuario;
	
	@NotBlank
	private String apellidoUsuario;
	
	@Email
	private String email;
	
	@NotBlank
	private String nombrePila;
	
	@NotBlank
	private String contra; 
	
	@NotBlank
	private String telefonoUsuario;
	
	private List<Integer> roles;
	
	
}
