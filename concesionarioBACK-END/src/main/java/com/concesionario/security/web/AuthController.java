package com.concesionario.security.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.concesionario.dto.Mensaje;
import com.concesionario.security.dto.JwtDto;
import com.concesionario.security.dto.LoginUsuario;
import com.concesionario.security.dto.NuevoUsuario;
import com.concesionario.security.jwt.JwtProvider;
import com.concesionario.security.logica.RolService;
import com.concesionario.security.logica.UsuarioService;
import com.concesionario.security.modelo.Rol;
import com.concesionario.security.modelo.Usuario;
import com.concesionario.security.modelo.UsuarioPrincipal;
import com.concesionario.security.persistencia.RolRepositorio;





@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	RolService rolService;
	
	@Autowired
	JwtProvider jwtProvider;
	
	@Autowired
	RolRepositorio rolRepositorio;
	
	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario , BindingResult bindingResult){
		if(bindingResult.hasErrors())
			return new ResponseEntity(new Mensaje("Campos mal puestos o email Invalido"), HttpStatus.BAD_REQUEST);
		if(usuarioService.existByNombreUsuarioComplejo(nuevoUsuario.getNombreUsuario()))
			return new ResponseEntity(new Mensaje("Ese nombre ya existe"), HttpStatus.BAD_REQUEST);
//		if(usuarioService.existByEmail(nuevoUsuario.getEmail()))
//			return new ResponseEntity(new Mensaje("Ese mail ya existe"), HttpStatus.BAD_REQUEST);
	
        Usuario usuario =
                new Usuario(nuevoUsuario.getNombreUsuario(), nuevoUsuario.getApellidoUsuario(), nuevoUsuario.getEmail(), nuevoUsuario.getNombrePila(), passwordEncoder.encode(nuevoUsuario.getContra()),nuevoUsuario.getTelefonoUsuario());
        	Set<Rol> roles = new HashSet<>();
        	System.out.println(nuevoUsuario.getRoles());
        	
        	for (Integer i :nuevoUsuario.getRoles() ) { 
        		usuario.getRoles().add(rolRepositorio.findById(i).orElse(null));
        	}
//        	
//    		roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
//    		if(nuevoUsuarioComplejo.getRoles().contains("admin"))
//    			roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
//        usuarioComplejo.setRoles(roles);
        usuarioService.save(usuario);
        return new ResponseEntity(new Mensaje("usuario guardado"), HttpStatus.CREATED);
    }
			
	
	
	@PostMapping("/login")
	public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult){
		if(bindingResult.hasErrors())
			return new ResponseEntity(new Mensaje("Campos mal puestos"), HttpStatus.BAD_REQUEST);
		Authentication authentication = 
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getContra()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateToken(authentication);
		UsuarioPrincipal usuarioPrincipal = (UsuarioPrincipal)authentication.getPrincipal();
		JwtDto jwtDto = new JwtDto(jwt,usuarioPrincipal.getUsername(), usuarioPrincipal.getAuthorities(),usuarioPrincipal.getId());
		return new ResponseEntity(jwtDto, HttpStatus.OK);                 
	 	}
	
	@GetMapping("/roles")
	public List<Rol> listarTodos() {
		return rolService.listarTodos();
	}
	
	@GetMapping("/usuarios")
	public List<Usuario> listarTodosUsuarios() {
		return usuarioService.listarTodosUsuarios();
	}
	
	@GetMapping("/usuario/{idUsuario}")
    public Usuario getUsuario(@PathVariable(name = "idUsuario") Integer idUsuario) {
        return usuarioService.getUsuario(idUsuario);
    }
	
	@RequestMapping(value = "/usuario", method = RequestMethod.PUT)
    public Usuario actualizar(@RequestBody Usuario usuario) { 
        return usuarioService.actualizar(usuario);
    }
	
	}

