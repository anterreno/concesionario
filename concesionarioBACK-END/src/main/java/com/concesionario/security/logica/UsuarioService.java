package com.concesionario.security.logica;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.security.modelo.Rol;
import com.concesionario.security.modelo.Usuario;
import com.concesionario.security.persistencia.UsuarioRepositorio;






@Service
@Transactional
public class UsuarioService {
	
	@Autowired
	UsuarioRepositorio repositorio;
	
	public Optional<Usuario>getByNombreUsuario(String nombreUsuario){
		return repositorio.findByNombreUsuario(nombreUsuario);
	}
	
	public Usuario getUsuario(Integer idUsuario) {
		return repositorio.findById(idUsuario).get();
	}
	
	public Usuario save(Usuario usuario) {
		return repositorio.save(usuario);
	}


	public void eliminar(Integer id) {
		repositorio.deleteById(id);
		}
	
	public boolean existByNombreUsuarioComplejo(String nombreUsuario) {
		return repositorio.existsByNombreUsuario(nombreUsuario);
	}
	
	public boolean existByEmail(String email) {
		return repositorio.existsByEmail(email);
	}

	public List<Usuario> listarTodosUsuarios() {
		return repositorio.findAll();
	}
	
	public boolean existByTelefonoUsuario(String telefonoUsuario) {
		return repositorio.existsByTelefonoUsuario(telefonoUsuario);
	}
	
	public Usuario actualizar(Usuario usuario) {
        if (usuario.getIdUsuario() == null) {
            throw new RuntimeException("Error el objeto NO tiene id");
         }
        return repositorio.save(usuario);
    }

}


