package com.concesionario.security.logica;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concesionario.security.modelo.Rol;
import com.concesionario.security.persistencia.RolRepositorio;

//import ar.com.proyectofinal.easyfutbol.security.enums.RolNombre;




@Service
@Transactional
public class RolService {
	
	@Autowired
	RolRepositorio rolRepositorio;
	
	public Optional<Rol> getByRolNombre(String rolNombre){
		return rolRepositorio.findByRolNombre(rolNombre);
		
	}
	
	public List<Rol> listarTodos() {
		return rolRepositorio.findAll();
	}
	
	public void save(Rol rol) {
	rolRepositorio.save(rol);
	}

}
