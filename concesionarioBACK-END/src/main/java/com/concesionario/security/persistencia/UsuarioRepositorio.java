package com.concesionario.security.persistencia;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.concesionario.security.modelo.Usuario;





@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario , Integer> {
	
//	Page<UsuarioComplejo> findByNombreUsuarioComplejoContainingIgnoreCase(String nombreUsuarioComplejo, Pageable pagina);
	
//	@Query("select u from UsuarioComplejo u where u.nombreUsuarioComplejo like %?1% or u.apellidoUsuarioComplejo like %?1%")
//	List<UsuarioComplejo> buscarPorNombreYApellido(String nombreUsuarioComplejo, String apellidoUsuarioComplejo);
	
	Optional<Usuario> findByNombreUsuario(String nombreUsuario);
	boolean existsByNombreUsuario(String nombreUsuario);
	boolean existsByEmail(String email);
	boolean existsByTelefonoUsuario(String telefonoUsuario);
}
