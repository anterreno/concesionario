package com.concesionario.security.persistencia;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.concesionario.security.modelo.Rol;






@Repository
public interface RolRepositorio extends JpaRepository<Rol, Integer> {
	Optional<Rol> findByRolNombre(String rolNombre);
	

}
