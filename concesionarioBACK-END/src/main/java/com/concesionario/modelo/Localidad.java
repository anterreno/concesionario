package com.concesionario.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class Localidad {
	@Id
	@GeneratedValue
	private Integer idLocalidad;
	private String nombreLocalidad;
	
//	@ManyToOne
//	private Provincia provincia;
}
