package com.concesionario.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class TipoCombustible {
	@Id
	@GeneratedValue
	private int id;
	private String nombre;

}
