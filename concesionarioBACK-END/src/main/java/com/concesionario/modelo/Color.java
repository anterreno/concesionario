package com.concesionario.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Color {
	
	@Id
	@GeneratedValue
	private Integer idColor;
	private String codigoPintura;
	private String nombreColor;

}
