package com.concesionario.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Motor {
	@Id
	@GeneratedValue
	private Integer id;
	private String cilindrada;
	private String potencia;	

}
