package com.concesionario.modelo;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class Vehiculo {
	@Id
	@GeneratedValue
	private Integer id;
	private Double precioLista;
	private Integer stock;
	private String descripcion;
	private Integer kms;
	
	@OneToOne
	private Color color;
	
	@OneToOne
	private Condicion condicion;
		
	@OneToOne
	private Marca marca;
	
	@OneToOne
	private Modelo modelo;
	
	@OneToOne
	private Motor motor;
		
	@OneToOne
	private TipoCombustible tipoCombustible;
	
	@OneToOne
	private TipoVehiculo tipoVehiculo;
	
	@OneToOne
	private Version version;
	
}
