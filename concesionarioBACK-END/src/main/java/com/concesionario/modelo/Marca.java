package com.concesionario.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Marca {
	
	@Id
	@GeneratedValue
	private Integer idMarca;
	private String nombreMarca;
	
	@OneToMany(mappedBy="marca", cascade=CascadeType.MERGE, fetch = FetchType.LAZY)
	private List<Modelo> modelos = new ArrayList<>();
	

}
