package com.concesionario.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Modelo {
	@Id
	@GeneratedValue
	private Integer idModelo;
	private String nombre;
	
	@ManyToOne
//	@JoinColumn(name="id_marca")
	@JsonIgnore
	private Marca marca;

	@OneToMany
	private List<Version> versiones = new ArrayList<>();
}
