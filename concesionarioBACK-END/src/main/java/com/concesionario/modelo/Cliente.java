package com.concesionario.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.sun.istack.NotNull;

import lombok.Data;

import com.concesionario.modelo.Localidad;
@Entity
@Data
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCliente;
		
	@Column(unique = true)
	private String nombreCliente;
	
	@NotNull
	@Column
	private String apellidoCliente;
	
	@NotNull
	@Column
	private String emailCliente;
	
	@NotNull
	@Column
	private String direccionCliente;
	
	@NotNull
	@Column
	private Integer dniCliente;
	
	@ManyToOne
	private Localidad localidad;
	
	@NotNull
	@Column
	private String telefonoCliente;

}
