package com.concesionario.modelo;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data

public class Version {
	@Id
	@GeneratedValue
	private int id;
	private String nombre;
	private String observacion;
	
	@ManyToOne
	private Modelo modelo;
	

}
