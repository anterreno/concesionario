package com.concesionario.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Provincia {
	@Id
	@GeneratedValue
	private Integer idProvincia;
	private String nombreLocalidad;
	
//	@OneToMany
//	private Localidad localidad;
}
