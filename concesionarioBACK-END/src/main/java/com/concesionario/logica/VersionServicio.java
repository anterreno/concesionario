package com.concesionario.logica;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Version;
import com.concesionario.persistencia.VersionRepositorio;
@Service
public class VersionServicio {
	@Autowired
	private VersionRepositorio versionRepositorio;
	
	public List<Version> listarTodas() {
		return versionRepositorio.findAll();
	}
	public Version getVersion(Integer id) {
		return versionRepositorio.findById(id).get();
	}
	public Version guardar(Version v) {
		return versionRepositorio.save(v);
	}
	public Version actualizar(Version v) {
		return versionRepositorio.save(v);
	}
	public void eliminar(Integer id) {
		versionRepositorio.deleteById(id);
	}
}
