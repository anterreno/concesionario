package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Marca;
import com.concesionario.persistencia.MarcaRepositorio;

@Service
public class MarcaServicio {
	@Autowired
	private MarcaRepositorio repositorio;

	public List<Marca> listarTodas() {
		Sort orden = Sort.by(Sort.Direction.ASC,"nombreMarca");
		return repositorio.findAll(orden);
	}
	
	public Marca getMarca(Integer idMarca) {
		return repositorio.findById(idMarca).get();
	}
	public Marca guardar(Marca m) {
		return repositorio.save(m);
	}
	public Marca actualizar(Marca m) {
		return repositorio.save(m);
	}
	public void eliminar(Integer idMarca) {
		repositorio.deleteById(idMarca);
	}
	
	public boolean existsByNombreMarca(String nombreMarca) {
		return repositorio.existsByNombreMarca(nombreMarca);
	}

}
