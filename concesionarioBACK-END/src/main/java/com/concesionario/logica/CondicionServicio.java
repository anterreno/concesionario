package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Condicion;
import com.concesionario.modelo.Version;
import com.concesionario.persistencia.CondicionRepositorio;
import com.concesionario.persistencia.VersionRepositorio;
@Service
public class CondicionServicio {
	@Autowired
	private CondicionRepositorio condicionRepositorio;
	
	public List<Condicion> listarTodas() {
		return condicionRepositorio.findAll();
	}
	public Condicion getCondicion(Integer id) {
		return condicionRepositorio.findById(id).get();
	}
	public Condicion guardar(Condicion c) {
		return condicionRepositorio.save(c);
	}
	public Condicion actualizar(Condicion c) {
		return condicionRepositorio.save(c);
	}
	public void eliminar(Integer id) {
		condicionRepositorio.deleteById(id);
	}

}
