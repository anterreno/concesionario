package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Color;
import com.concesionario.persistencia.ColorRepositorio;

@Service
public class ColorServicio {
	@Autowired
	private ColorRepositorio repositorio;

	public List<Color> listarTodos() {
		Sort orden = Sort.by(Sort.Direction.ASC,"nombreColor");
		return repositorio.findAll(orden);
	}
	
	public Color getColor(Integer idColor) {
		return repositorio.findById(idColor).get();
	}
	public Color guardar(Color c) {
		return repositorio.save(c);
	}
	public Color actualizar(Color c) {
		return repositorio.save(c);
	}
	public void eliminar(Integer idColor) {
		repositorio.deleteById(idColor);
	}
	public boolean existsByNombreColor(String nombreColor) {
		return repositorio.existsByNombreColor(nombreColor);
	}
}
