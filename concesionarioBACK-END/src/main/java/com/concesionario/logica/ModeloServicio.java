package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Modelo;
import com.concesionario.persistencia.ModeloRepositorio;

@Service
public class ModeloServicio {
	@Autowired
	private ModeloRepositorio repositorio;
	
	public List<Modelo> listarTodos() {
		return repositorio.findAll();
	}
	
	public Modelo getModelo(Integer idModelo) {
		return repositorio.findById(idModelo).get();
	}
	
	public Modelo guardar(Modelo m) {
		return repositorio.save(m);
	}
	
	public Modelo actualizar(Modelo m) {
		return repositorio.save(m);
	}
	public void eliminar(Integer idModelo) {
		repositorio.deleteById(idModelo);
	}
	public boolean existsByNombre(String nombre) {
		return repositorio.existsByNombre(nombre);
	}
//	public Iterable<Modelo> getModeloPorVehiculoId(Integer idVehiculo) {
//		return repositorio.findByVehiculo_IdVehiculo(idVehiculo);
//	}
	
}


