package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Condicion;
import com.concesionario.modelo.TipoCombustible;
import com.concesionario.persistencia.CondicionRepositorio;
import com.concesionario.persistencia.TipoCombustibleRepositorio;
@Service
public class TipoCombustibleServicio {
	@Autowired
	private TipoCombustibleRepositorio tipoCombustibleRepositorio;
	
	public List<TipoCombustible> listarTodas() {
		return tipoCombustibleRepositorio.findAll();
	}
	public TipoCombustible getTipoCombustible(Integer id) {
		return tipoCombustibleRepositorio.findById(id).get();
	}
	public TipoCombustible guardar(TipoCombustible tc) {
		return tipoCombustibleRepositorio.save(tc);
	}
	public TipoCombustible actualizar(TipoCombustible tc) {
		return tipoCombustibleRepositorio.save(tc);
	}
	public void eliminar(Integer id) {
		tipoCombustibleRepositorio.deleteById(id);
	}
}
