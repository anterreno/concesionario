package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.TipoCombustible;
import com.concesionario.modelo.TipoVehiculo;
import com.concesionario.persistencia.TipoCombustibleRepositorio;
import com.concesionario.persistencia.TipoVehiculoRepositorio;
@Service
public class TipoVehiculoServicio {
	@Autowired
	private TipoVehiculoRepositorio tipoVehiculoRepositorio;
	
	public List<TipoVehiculo> listarTodas() {
		return tipoVehiculoRepositorio.findAll();
	}
	public TipoVehiculo getTipoVehiculo(Integer id) {
		return tipoVehiculoRepositorio.findById(id).get();
	}
	public TipoVehiculo guardar(TipoVehiculo tv) {
		return tipoVehiculoRepositorio.save(tv);
	}
	public TipoVehiculo actualizar(TipoVehiculo tv) {
		return tipoVehiculoRepositorio.save(tv);
	}
	public void eliminar(Integer id) {
		tipoVehiculoRepositorio.deleteById(id);
	}
}
