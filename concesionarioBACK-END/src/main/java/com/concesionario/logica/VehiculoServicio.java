package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Vehiculo;
import com.concesionario.persistencia.VehiculoRepositorio;


@Service
public class VehiculoServicio {
	@Autowired
	private VehiculoRepositorio vehiculoRepositorio;
	
	public List<Vehiculo> listarTodos() {
		return vehiculoRepositorio.findAll();
	}
	public Vehiculo getVehiculo(Integer id) {
		return vehiculoRepositorio.findById(id).get();
	}
	public Vehiculo guardar(Vehiculo v) {
		return vehiculoRepositorio.save(v);
	}
	public Vehiculo actualizar(Vehiculo v) {
		return vehiculoRepositorio.save(v);
	}
	public void eliminar(Integer id) {
		vehiculoRepositorio.deleteById(id);
	}

}
