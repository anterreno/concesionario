package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Motor;
import com.concesionario.persistencia.MotorRepositorio;

@Service
public class MotorServicio {
	@Autowired
	private MotorRepositorio repositorio;
	
	public List<Motor> listarTodos() {
		return repositorio.findAll();
	}
	
	public Motor getMotor(Integer id) {
		return repositorio.findById(id).get();
	}
	
	public Motor guardar(Motor m) {
		return repositorio.save(m);
	}
	
	public Motor actualizar(Motor m) {
		return  repositorio.save(m);
	}
	public void eliminar(Integer id) {
		repositorio.deleteById(id);
	}
//	public boolean existsByNombre(String nombreMotor) {
//		return repositorio.existsByNombreMotor(nombreMotor);
//	}
	
//	public Iterable<Motor> getMotorPorVehiculoId(Integer idVehiculo) {
//	return repositorio.findByVehiculo_IdVehiculo(idVehiculo);
//	}
	
}