package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Localidad;
import com.concesionario.persistencia.LocalidadRepositorio;


@Service
public class LocalidadServicio {
	
	@Autowired
	private LocalidadRepositorio repositorio;
	
	public List<Localidad> listarTodas() {
		return repositorio.findAll();
	}

}
