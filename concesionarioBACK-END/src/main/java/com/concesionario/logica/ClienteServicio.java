package com.concesionario.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concesionario.modelo.Cliente;
import com.concesionario.persistencia.ClienteRepositorio;

@Service
public class ClienteServicio {

	@Autowired
	private ClienteRepositorio clienteRepositorio; 
	
	public List<Cliente> listarTodos() {
		return clienteRepositorio.findAll();
	}
	
	public Cliente getCliente(Integer idCliente) {
		return clienteRepositorio.findById(idCliente).get();
	}
	
	public Cliente guardar(Cliente c) {
		return clienteRepositorio.save(c);
	}
	
	public Cliente actualizar(Cliente c) {
		return  clienteRepositorio.save(c);
	}
	public void eliminar(Integer idCliente) {
		clienteRepositorio.deleteById(idCliente);
	}
	public List<Cliente> getClientesPorId(Integer idCliente) {
		return clienteRepositorio.findByIdCliente(idCliente);
	}
	
	public List<Cliente> getClientesPorNombre(String nombreCliente) {
		return clienteRepositorio.findByNombreCliente(nombreCliente);
	}
	
	public boolean existsByDniCliente(Integer dniCliente) {
		return clienteRepositorio.existsByDniCliente(dniCliente);
	}
}
